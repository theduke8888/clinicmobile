package com.example.mobileproject;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DoctorLoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etEmail, etPassword;
    private Button btnLogin;
    private ProgressDialog pgDialog;
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_login);

        setTitle("Doctor Login");
        if (SharedPreferenceManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, DoctorReservationsActivity.class));
            return;
        }

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        pgDialog = new ProgressDialog(this);
        pgDialog.setMessage("Please wait...");
        btnLogin.setOnClickListener(this);
    }
    private void doctorLogin() {
        final String doctor_email = etEmail.getText().toString();
        final String doctor_password = etPassword.getText().toString();

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TAG", "Register Response: " + response.toString());
                        pgDialog.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (!obj.getBoolean("error")) {
                                SharedPreferenceManager.getInstance(
                                        getApplicationContext()).doctorLogin(obj.getInt("id"),
                                        obj.getString("name"),
                                        obj.getString("license"),
                                        obj.getString("email"),
                                        obj.getString("contact"),
                                        obj.getString("address"),
                                        obj.getInt("field_id"));
                                startActivity(new Intent(getApplicationContext(), DoctorReservationsActivity.class));
                                finish();

                            }
                            else  {
                                builder = new AlertDialog.Builder(DoctorLoginActivity.this);
                                builder
                                        .setTitle("Doctor Login")
                                        .setMessage(obj.getString("message"))
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        });
                                 alertDialog = builder.create();
                                 alertDialog.show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email",doctor_email);
                params.put("password", doctor_password);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void onClick(View v) {
        if (v == btnLogin) {
            doctorLogin();
        }
    }
}
