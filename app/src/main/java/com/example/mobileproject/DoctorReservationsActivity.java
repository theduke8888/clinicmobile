package com.example.mobileproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class DoctorReservationsActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView textViewDoctorName, textViewDoctorField, textViewDoctorLicense,
            textViewDoctorTodayAppointments, textViewDoctorComingAppointments;

    private Button buttonTodaysAppointments, buttonComingAppointments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_reservations);
        setTitle("Available Reservations");


        getDoctorField();
        getNumberOfAppointments();

        //TextView
        textViewDoctorName = (TextView)  findViewById(R.id.textViewDoctorName);
        textViewDoctorField = (TextView) findViewById(R.id.textViewDoctorField);
        textViewDoctorLicense = (TextView) findViewById(R.id.textViewDoctorLicense);
        textViewDoctorTodayAppointments = (TextView) findViewById(R.id.textViewDoctorTodayAppointments);
        textViewDoctorComingAppointments = (TextView) findViewById(R.id.textViewDoctorComingAppointments);

        textViewDoctorName.setText(SharedPreferenceManager.getInstance(this).getDoctorName());
        textViewDoctorLicense.setText("License Number : " + SharedPreferenceManager.getInstance(this).getDoctorLicense());

        //Buttons
        buttonTodaysAppointments = (Button) findViewById(R.id.buttonDoctorTodaysAppointments);
        buttonComingAppointments = (Button) findViewById(R.id.buttonDoctorsComingAppointments);
        buttonTodaysAppointments.setOnClickListener(this);
        buttonComingAppointments.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == buttonTodaysAppointments)
            startActivity(new Intent(this, TodaysAppointmentsActivity.class));

        if (view == buttonComingAppointments)
            startActivity(new Intent(this, ComingAppointmentsActivity.class));

    }

    public void getNumberOfAppointments() {
        final int doctor_id = SharedPreferenceManager.getInstance(getApplicationContext()).getDoctorId();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.DISPLAY_NUMBER_OF_APPOINTMENTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String ta = jsonObject.getString("todays_appointments");
                            String ca = jsonObject.getString("coming_appointments");
                            textViewDoctorTodayAppointments.setText(ta);
                            textViewDoctorComingAppointments.setText(ca);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("doctor_id", String.valueOf(doctor_id));
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

    }

    public void getDoctorField() {
        final int field_id = SharedPreferenceManager.getInstance(this).getDoctorFieldId();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.DISPLAY_DOCTOR_FIELD_URL ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject  jsonObject = new JSONObject(response);
                            SharedPreferenceManager.getInstance(getApplicationContext()).doctorField(jsonObject.getString("area"));
                            textViewDoctorField.setText("Specialty : " + SharedPreferenceManager.getInstance(getApplicationContext()).getDoctorField());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i("message", e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

        ){
            @Override
            protected Map<String, String > getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("field_id", String.valueOf(field_id));
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.mnuLogout:
                SharedPreferenceManager.getInstance(this).logout();
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;

        }
        return true;
    }
}
