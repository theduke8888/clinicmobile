package com.example.mobileproject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterPatients extends RecyclerView.Adapter<AdapterPatients.ViewHolder> {

    private List<ListPatient> listPatients;
    private Context context;

    public AdapterPatients(List<ListPatient> listPatients, Context context) {
        this.listPatients = listPatients;
        this.context = context;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_appointments, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ListPatient listPatient = listPatients.get(position);

        holder.textViewPatientName.setText(listPatient.getName());
        holder.textViewPatientAddress.setText(listPatient.getName());
        String stringDoa = listPatient.getDoa();
        SimpleDateFormat doaFrom = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat doaTo = new SimpleDateFormat("EEEE, MMMM d, yyyy");
        try {
            Date doa = doaFrom.parse(stringDoa);
            holder.textViewPatientDoa.setText(doaTo.format(doa));
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getItemCount() {
        return listPatients.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder {
        public TextView textViewPatientName, textViewPatientAddress, textViewPatientDoa;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewPatientName = (TextView) itemView.findViewById(R.id.textViewPatientName);
            textViewPatientAddress = (TextView) itemView.findViewById(R.id.textViewPatientAddress);
            textViewPatientDoa = (TextView) itemView.findViewById(R.id.textViewPatientDoa);
        }
    }
}
