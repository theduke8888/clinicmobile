package com.example.mobileproject;

public class ListPatient {
    private String name;
    private String address;
    private String doa;

    public ListPatient(String name, String address, String doa) {
        this.name = name;
        this.address = address;
        this.doa = doa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDoa() {
        return doa;
    }

    public void setDoa(String doa) {
        this.doa = doa;
    }
}
