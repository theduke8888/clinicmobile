package com.example.mobileproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterDoctor extends RecyclerView.Adapter<AdapterDoctor.ViewHolder> {
  //Generate constructors of List and context
  private List<ListDoctor> listDoctors;
  private Context context;

  public AdapterDoctor(List<ListDoctor> listDoctors, Context context) {
    this.listDoctors = listDoctors;
    this.context = context;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_doctors, parent, false);
    return new ViewHolder(v);

  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    ListDoctor listDoctor = listDoctors.get(position);

    //holder.tvName.setText(listDoctor.getName());
    //holder.tvAddress.setText(listDoctor.getAddress());
    holder.tvDoctorsName.setText(listDoctor.getName());
    holder.tvDoctorsArea.setText(listDoctor.getArea());
    holder.tvDoctorsContact.setText(listDoctor.getContact());
    holder.tvDoctorsEmail.setText(listDoctor.getEmail());
  }

  @Override
  public int getItemCount() {
    return listDoctors.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView tvDoctorsName, tvDoctorsArea, tvDoctorsEmail, tvDoctorsContact;

    public ViewHolder(@NonNull View itemView) {
      super(itemView);

      tvDoctorsName = (TextView) itemView.findViewById(R.id.tvDoctorsName);
      tvDoctorsArea= (TextView) itemView.findViewById(R.id.tvDoctorsArea);
      tvDoctorsEmail = (TextView) itemView.findViewById(R.id.tvDoctorsEmail);
      tvDoctorsContact = (TextView) itemView.findViewById(R.id.tvDoctorsContact);
    }
  }
}
