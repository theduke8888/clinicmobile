package com.example.mobileproject;


import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceManager {
    private  static SharedPreferenceManager mInstance;
    private static Context mCtx;
    public static final String SHARED_PREF_NAME ="mypref";
    private static final String KEY_DOCTOR_ID = "doctor_id";
    private static final String KEY_DOCTOR_NAME = "doctor_name";
    private static final String KEY_DOCTOR_LICENSE = "doctor_license";
    private static final String KEY_DOCTOR_EMAIL = "doctor_email";
    private static final String KEY_DOCTOR_CONTACT = "doctor_contact";
    private static final String KEY_DOCTOR_ADDRESS = "doctor_address";
    private static final String KEY_DOCTOR_FIELD_ID = "doctor_field_id";
    private static final String KEY_DOCTOR_FIELD = "doctor_area";
    private static final String KEY_DOCTOR_NUMBER_TODAY_APPOINTMENTS = "doctor_today_appointments";
    private static final String KEY_DOCTOR_NUMBER_COMING_APPONTMETNS = "doctor_coming_appointments";

    SharedPreferenceManager (Context context) {
        mCtx = context;
    }

    public static  synchronized SharedPreferenceManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreferenceManager(context);
        }
        return mInstance;
    }

    public boolean doctorLogin(int id, String name, String license, String email, String contact, String address, int field_id) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_DOCTOR_ID, id);
        editor.putString(KEY_DOCTOR_NAME, name);
        editor.putString(KEY_DOCTOR_LICENSE, license);
        editor.putString(KEY_DOCTOR_EMAIL, email);
        editor.putString(KEY_DOCTOR_CONTACT, contact);
        editor.putString(KEY_DOCTOR_ADDRESS, address);
        editor.putInt(KEY_DOCTOR_FIELD_ID, field_id);
        editor.apply();
        return true;
    }

    public boolean doctorField(String area) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =  sharedPreferences.edit();
        editor.putString(KEY_DOCTOR_FIELD, area);
        editor.apply();
        return true;
    }

    public boolean doctorAppointments(int numberTodaysAppointments, int numberComingAppointments) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_DOCTOR_NUMBER_COMING_APPONTMETNS, numberComingAppointments);
        editor.putInt(KEY_DOCTOR_NUMBER_TODAY_APPOINTMENTS, numberTodaysAppointments);
        editor.apply();
        return true;
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences.getString(KEY_DOCTOR_EMAIL, null) != null)
            return true;
        else
            return false;
    }

    public boolean logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    public int getDoctorId() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_DOCTOR_ID, 0);
    }

    public String getDoctorName() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DOCTOR_NAME, null);
    }

    public  String getDoctorLicense() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DOCTOR_LICENSE, null);
    }


    public String getDoctorEmail() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DOCTOR_EMAIL, null);
    }

    public String getDoctorContact() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DOCTOR_CONTACT, null);
    }

    public  String getDoctorAddress() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DOCTOR_ADDRESS, null);
    }


    public int getDoctorFieldId() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_DOCTOR_FIELD_ID, 0);
    }


    public String getDoctorField() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DOCTOR_FIELD, null);
    }

    public String getDoctorNumberTodayAppointments() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DOCTOR_NUMBER_TODAY_APPOINTMENTS, null);
    }

    public String getDoctorNumberComingAppointments() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_DOCTOR_NUMBER_COMING_APPONTMETNS, null);
    }


}
