package com.example.mobileproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnDoctorLogin, btnViewDoctors, btnPatientReservation;
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (SharedPreferenceManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, DoctorReservationsActivity.class));
            return;
        }

        btnDoctorLogin = (Button) findViewById(R.id.btnDoctorLogin);
        btnPatientReservation = (Button) findViewById(R.id.btnPatientReservation);
        btnViewDoctors = (Button) findViewById(R.id.btnViewDoctors);

        btnDoctorLogin.setOnClickListener(this);
        btnViewDoctors.setOnClickListener(this);
        btnPatientReservation.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == btnDoctorLogin) {
            startActivity(new Intent(this, DoctorLoginActivity.class));
        }
        else if (v == btnPatientReservation)
            startActivity(new Intent(this, PatientReservationActivity.class));
        else if (v== btnViewDoctors)
            startActivity(new Intent(this, ViewDoctorsActivity.class));
    }
}
