package com.example.mobileproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.media.tv.TvContract;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViewDoctorsActivity extends AppCompatActivity {

  private RecyclerView recyclerView;
  private RecyclerView.Adapter adapter;
  private List<ListDoctor> listDoctors;
//    private String  URL_Doctors = "http://192.168.0.13/mobileProject/viewAllDoctors.php";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_view_doctors);

    recyclerView = (RecyclerView) findViewById(R.id.rvDoctors);
    recyclerView.setHasFixedSize(true); //every item in the recycler view has fix size
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    listDoctors = new ArrayList<>();


    getListDoctors();

  }

  private void getListDoctors() {
    final ProgressDialog pgDialog = new ProgressDialog(this);
    pgDialog.setMessage("Data Loading");
    pgDialog.show();
    StringRequest stringRequest = new StringRequest(Request.Method.GET,
            Constants.DISPLAY_DOCTORS_URL,
            new Response.Listener<String>() {
              @Override
              public void onResponse(String response) {
                pgDialog.dismiss();
                try {
                  JSONObject obj = new JSONObject(response);
                  JSONArray array = obj.getJSONArray("list_doctors");
                  for(int i = 0; i < array.length(); i++) {
                    JSONObject o = array.getJSONObject(i);
                    ListDoctor doctor = new ListDoctor(
                            o.getString("name"),
                            o.getString("area"),
                            o.getString("email"),
                            o.getString("contact")
                    );
                    listDoctors.add(doctor);
                  }
                  adapter = new AdapterDoctor(listDoctors, getApplicationContext());
                  recyclerView.setAdapter(adapter);

                } catch (JSONException e) {
                  e.printStackTrace();
                }
              }
            },
            new Response.ErrorListener() {
              @Override
              public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
              }
            }
    );
    RequestQueue requestQueue = Volley.newRequestQueue(this);
    requestQueue.add(stringRequest);
  }

}
