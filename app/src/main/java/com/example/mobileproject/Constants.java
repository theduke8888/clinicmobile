package com.example.mobileproject;

public class Constants {
    public static final String ROOT_URL = "https://gibemobileservice.herokuapp.com/";
    public static final String LOGIN_URL = ROOT_URL+"doctorLogin.php";
    public static final String DISPLAY_DOCTORS_URL = ROOT_URL+"viewAllDoctors.php";
    public static final String CREATE_APPOINTMENT_URL  = ROOT_URL+"createAppointment.php";
    public static final String DISPLAY_DOCTOR_FIELD_URL = ROOT_URL+"viewDoctorField.php";
    public static final String DISPLAY_NUMBER_OF_APPOINTMENTS = ROOT_URL+"viewNumberOfAppointments.php";
    public static final String DISPLAY_TODAYS_APPOINTMENTS_URL = ROOT_URL+"viewTodaysAppointments.php";
    public static final String DISPLAY_COMING_APPOINTMETNS_URL = ROOT_URL+"viewComingAppointments.php";


}
