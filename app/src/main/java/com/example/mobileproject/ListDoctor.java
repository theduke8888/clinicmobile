package com.example.mobileproject;

public class ListDoctor {
  private String name;
  private String area;
  private String email;
  private String contact;

  public ListDoctor(String name, String area, String email, String contact) {
    this.name = name;
    this.area = area;
    this.email = email;
    this.contact = contact;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }
}
