package com.example.mobileproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Analyzer;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.tv.TvContract;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AnalogClock;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SimpleTimeZone;

import java.util.HashMap;


public class PatientReservationActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private Button btnReservation;
    private Spinner spDoctors;
    private List<ListDoctorsInReservation> listDoctorsInReservationList;
    private ArrayList<ListDoctorsInReservation> doctors = new ArrayList<ListDoctorsInReservation>();
    private RadioGroup rgGender;
    private RadioButton rbGender;
    private EditText etDoa, edDob, etDob;
    private DatePickerDialog datePickerDialogDoa, datePickerDialogDob;
    //   private Date doa, dob;
    private Calendar calendar;
    private EditText editTextPatientName, editTextPatentAddress, editTextPatientContact,
            editTextPatientJob, editTextPatientDoctor_id;
    private ListDoctorsInReservation listDoctorsInReservationDoctor_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_reservation);
        btnReservation = (Button) findViewById(R.id.btnReservation);
        btnReservation.setOnClickListener(this);

        // Spinner for Doctors
        spDoctors = (Spinner) findViewById(R.id.spDoctors);
        spDoctors.setOnItemSelectedListener(this);

        // Radio group for gender
        rgGender = (RadioGroup) findViewById(R.id.rgGender);
        rgGender.clearCheck();

        //Date of Appointment
        etDoa = (EditText) findViewById(R.id.etDoa);
        etDoa.setOnClickListener(this);

        //Date of Birth
        etDob = (EditText) findViewById(R.id.etDob);
        etDob.setOnClickListener(this);

        getDoctors();
    }

    private void getDoctors() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Retrieving Doctors...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                Constants.DISPLAY_DOCTORS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            listDoctorsInReservationList = new ArrayList<>();
                            JSONObject object = new JSONObject(response);
                            JSONArray array = object.getJSONArray("list_doctors");
                            ListDoctorsInReservation doctor = new ListDoctorsInReservation(0, "Choose Doctor", "XXX");
                            listDoctorsInReservationList.add(doctor);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject o = array.getJSONObject(i);
                                doctor = new ListDoctorsInReservation(
                                        o.getInt("id"),
                                        o.getString("name"),
                                        o.getString("area")
                                );
                                listDoctorsInReservationList.add(doctor);
                                doctors.add(listDoctorsInReservationList.get(i));
                            }
                            ArrayAdapter<ListDoctorsInReservation> adapter = new ArrayAdapter<ListDoctorsInReservation>(PatientReservationActivity.this, android.R.layout.simple_spinner_dropdown_item, doctors);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spDoctors.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(PatientReservationActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
        );
        RequestQueue requestQueue  = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        listDoctorsInReservationDoctor_id = (ListDoctorsInReservation) parent.getItemAtPosition(position);
        //  String sId = Integer.toString(ds.getId());
        //  Toast.makeText(PatientReservationActivity.this, sId, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

        if (v == btnReservation) {
            //Extract EditText
            String dob=null, doa=null, gender=null;
            int radioGenderId, doctor_id;
            editTextPatientName = (EditText) findViewById(R.id.etPatientName);
            editTextPatentAddress = (EditText) findViewById(R.id.etPatientAddress);
            editTextPatientContact = (EditText) findViewById(R.id.etPatientContact);
            editTextPatientJob = (EditText) findViewById(R.id.etPatientJob);
            if (!(etDob.getText().toString().isEmpty())) {
                dob = datePickerDialogDoa.getDatePicker().getYear() + "-" +
                        (datePickerDialogDob.getDatePicker().getMonth() + 1 )  + "-" +
                        datePickerDialogDob.getDatePicker().getDayOfMonth();
            }
            if (!(etDoa.getText().toString().isEmpty())) {
                doa = datePickerDialogDoa.getDatePicker().getYear() + "-" +
                        (datePickerDialogDoa.getDatePicker().getMonth() + 1 ) + "-" +
                        datePickerDialogDoa.getDatePicker().getDayOfMonth();
            }

            if (rgGender.getCheckedRadioButtonId() != -1) {
                radioGenderId = rgGender.getCheckedRadioButtonId();
                rbGender = (RadioButton) findViewById(radioGenderId);
                gender = rbGender.getText().toString();
            }
            doctor_id = listDoctorsInReservationDoctor_id.getId();

            final String name = editTextPatientName.getText().toString();
            String address = editTextPatentAddress.getText().toString();
            String contact = editTextPatientContact.getText().toString();
            String job = editTextPatientJob.getText().toString();

            createAppointment(name, address, contact, dob, job, doa, gender, doctor_id);
        }

        if (v == etDoa) {
            convertDateOfAppointment();
        }

        if (v == etDob) {
            convertDateOfBirth();
        }
    }

    public void convertDateOfBirth() {
        calendar  = Calendar.getInstance();
        final int y = calendar.get(Calendar.YEAR);
        final int m = calendar.get(Calendar.MONTH);
        final int d = calendar.get(Calendar.DAY_OF_MONTH);
        etDob.setText("");
        datePickerDialogDob = new DatePickerDialog(PatientReservationActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM d, ''yy");
                        Date date  = new Date(year, month, dayOfMonth);
                        String dateOfWeek = simpleDateFormat.format(date);
                        etDob.setText(dateOfWeek);
                    }
                }, y, m, d);
        calendar.add(Calendar.DATE, 0);
        Date today_date = calendar.getTime();
        datePickerDialogDob.getDatePicker().setMaxDate(today_date.getTime());
        datePickerDialogDob.show();
    }

    public void convertDateOfAppointment() {
        calendar = Calendar.getInstance();
        final int y = calendar.get(Calendar.YEAR);
        final int m = calendar.get(Calendar.MONTH);
        final int d = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialogDoa = new DatePickerDialog(PatientReservationActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        SimpleDateFormat formatDate = new SimpleDateFormat("MMMM d, ''yy");
                        Date date = new Date(year, month, dayOfMonth);
                        String dateOfWeek = formatDate.format(date);
                        etDoa.setText(dateOfWeek);

                    }
                }, y, m, d);
        calendar.add(Calendar.DAY_OF_WEEK, 0);
        Date today_date = calendar.getTime();
        datePickerDialogDoa.getDatePicker().setMinDate(today_date.getTime());
        datePickerDialogDoa.show();
    }

    public void createAppointment(String n, String a, String c, String db, String j, String da, String g, int d_id) {
        final String name = n;
        final String address = a;
        final String contact = c;
        final String dob = db;
        final String job = j;
        final String doa = da;
        final String gender = g;
        final int doctor_id = d_id;
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating Appointment");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.CREATE_APPOINTMENT_URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            showAlertDialog(jsonObject.getString("message"), 1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        showAlertDialog("All field are required", 2);
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("address", address);
                params.put("contact", contact);
                params.put("dob", dob);
                params.put("job", job);
                params.put("doa", doa);
                params.put("gender", gender);
                params.put("doctor_id", String.valueOf(doctor_id));
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void showAlertDialog(String message, final int type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Nueva Clinica")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (type == 1) {
                            PatientReservationActivity.this.finish();
                        }
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}

